define(function (require, exports, module) {
    "use strict";

    var CommandManager = brackets.getModule("command/CommandManager"),
        EditorManager = brackets.getModule("editor/EditorManager"),
        Menus = brackets.getModule("command/Menus");


    var MY_COMMAND_ID = "tuxianof.codeCraft.com.about";
    CommandManager.register("About", MY_COMMAND_ID, aboutCodeCraft);

    Menus.addMenu("CodeCraft","tuxianof.codecraft.com");
    var menu = Menus.getMenu("tuxianof.codecraft.com");
    menu.addMenuItem(MY_COMMAND_ID);

    // Function to run when the menu item is clicked
    function aboutCodeCraft() {
        var editor = EditorManager.getFocusedEditor();
        if (editor) {
            var insertionPos = editor.getCursorPos();
            editor.document.replaceRange("Hello, world!", insertionPos);
        }
    }
});